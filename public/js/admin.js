(function ( $ ) { 

    // put all that "wl_alert" code here   
    $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)')
                              .exec(window.location.href);
            return (results == null)?0:(results[1] || 0);
        }
    

    $( document ).ready(function() {

        // Setup - add a text input to each footer cell
        $('#paginatedTable tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" id="'+title+'" placeholder="Search '+title+'" />' );

        } );



        var table = $('#paginatedTable').DataTable( {
            "processing": true,
            "serverSide": true,
            "pageLength": 5,
            "ajax": {
                "url": "http://4.28.134.168:8080/product/list/datatable",
                // "url": "data/get.json",
                "data": function ( data ) {
                 //process data before sent to server.
             }},
            "columns": [
                        { "data": "prodCode", "name" : "prodCode", "title" : "prodCode", 
                            "render": function ( data, type, row, meta ) 
                            {
                              return '<a href="'+window.location.origin+'/prodCode='+data.trim()+'">'+data+'</a>';
                            }  
                        },
                        { "data": "description", "name" : "description" , "title" : "description"},
                        { "data": "vendorName", "name" : "vendorName" , "title" : "vendorName"},
                        { "data": "productCategory", "name" : "productCategory" , "title" : "productCategory"},
                        { "data": "department", "name" : "department" , "title" : "productDepartment"}
                    ]    
        });
        
        $('#paginatedTable').dataTable().fnSetFilteringEnterPress();

        // Apply the search
        table.columns().every( function () {
            var that = this;
     
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );

            // if param found now, do search now
            if($.urlParam($( 'input', this.footer() )[0].id)){
                $( 'input', this.footer() )[0].value = $.urlParam($( 'input', this.footer() )[0].id);
                that.search( $( 'input', this.footer() )[0].value ).draw();
            }

        } );

        

    });
}( jQuery ));