import React from 'react';
import PropTypes from 'prop-types';
import Request from 'superagent';
import {
    FormGroup, FormControl, Form, Checkbox, ControlLabel, ButtonToolbar, Button, Radio,
    Col, Grid, Row, Modal, Table, ProgressBar} from 'react-bootstrap';
import {SelectList} from "./SelectList";
import DatePicker from 'react-datepicker';
import Select from 'react-select';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-select/dist/react-select.css';
import * as moment from "moment";
import apiUrl from "./BaseUrls";


export class ProductInfo extends React.Component{
    constructor(props){
        super(props);
        // if(props.prodDetails.length>0){
        // var prod=props.prodDetails[0];
        // }copyAllotment=true&copyPricing=true&copyDefaultCancellation=true&copyWebAccess=true
        this.state={
            temp:{},
            saveFlag:'',
            cloneModal:false,
            newProduct:false,
            restrictChange:false,
            mealPlan:[],
            roomType:[],
            vendorList:[],
            allotCode:[],
            copyFlags:{
                copyAllotment:false,
                copyPricing:false,
                copyDefaultCancellation:false,
                copyWebAccess:false
            },
            product:{
                prodCode:'',
                description: '',
                displayCategory: '',
                vendor: {
                    vendorID:'',
                    vendorName:'',
                },
                webPriority: '',
                allotCode: '',
                productDescription:'',
                calendarPriority: '',
                permRoomCode: '',
                specialRoomFlag: '',
                extranet: '',
                roomTypeUpdateFlag: '',
                transferType: '',
                notifyEmails: '',
                lastCalendarUpdate: null,
                productConfiguration: {
                    multiple: '',
                    confType: '',
                    markTUISpain: '',
                    markTUIGermany: '',
                    htmlFile: '',
                    associated: '',
                    hotel: ''
                },
                productFlightInfo: {
                    flight1: '',
                    flight2: '',
                    flight3: '',
                    flight4: ''
                },
                productFlags: {
                    misc1Flag: '',
                    misc2Flag: '',
                    pULocFlag: '',
                    pUTimeFlag: '',
                    flightItinFlag: '',
                    packageFlag: '',
                    allotmentFlag: '',
                    priceOverrideFlag: '',
                    displayOccFlag: '',
                    displayNightsFlag: '',
                    reportAllotUsageFlag: ''
                },
                productInstructions: {
                    instr1: '',
                    instr2: '',
                    instr3: '',
                    instr4: '',
                    instr5: ''
                },
                webAccess: '',
                promotionalDetails: {
                    brochureData: '',
                    eliteBrochureData: '',
                    freeSellProgram: '',
                    freeSellNotes: '',
                    usesLanguage: ''
                },
                dept: {
                    id: '',
                    label: ''
                },
                cancellationPolicies:[],
                priceProperties: {
                    needsPriceUpdate: '',
                    productCharge:'',
                    nonRefundable: '',
                    prodCom: ''
                },
                mealPlan: {
                    mealPlanID: '',
                    mealPlan: '',
                    dateUpdated: null,
                    deleted: '',
                    otaMealPlanCode: ''
                },
                roomType: {
                    roomTypeId: '',
                    roomType: '',
                    dateUpdated: '',
                    deleted: '',
                    hiltonRoomTypeId: '',
                    hiltonHotelCodes: '',
                    externalRoomTypeId: ''
                },
                groupPricingType: '',
                webDuration: {
                    startDate: null,
                    endDate: null
                },
                occupancyConfiguration: {
                    defaultNights: '',
                    minNights: '',
                    maxOcc: '',
                    maxChildAge: ''
                },
                resortFee: {
                    resortFeeID: '',
                    resortFeeType: '',
                    resortFee: ''
                },
                roomTypeModified: '',
                notifyDuration: {
                    startDate: null,
                    endDate: null
                },
                disneyInformation: {
                    reportDisneyTicketFlag: '',
                    disneyHotelCode: '',
                    disneyRoomType: '',
                    disneyCodeCurrentYear: '',
                    disneyCodeNextYear: '',
                    disneyPackages: [
                        {
                            ordinal: 1,
                            disneyPackageCode: '',
                            disneyAdultTicketCode: '',
                            disneyChildTicketCode: '',
                            disneyCodeDuration: {
                                startDate: null,
                                endDate: null
                            }
                        },
                        {
                            ordinal: 2,
                            disneyPackageCode: '',
                            disneyAdultTicketCode: '',
                            disneyChildTicketCode: '',
                            disneyCodeDuration: {
                                startDate: null,
                                endDate: null
                            }
                        }
                    ],
                    latestDisneyCalendar: '',
                    latestDisneyCalendarUpdate: '',
                    reportDisneyFlag: ''
                },
                productCategory: {
                    id: '',
                    label: ''
                },
                restrictionProperties: {
                    dateChangeRestricted: true,
                    occChangeRestricted: true,
                    nightsChangeRestricted: true,
                    qtyChangeRestricted: true,
                    typeChangeRestricted: true,
                    priceChangeRestricted: true,
                    paxChangeRestricted: true,
                    webRestricted: true,
                    b2cRestricted: true
                },
                modifierType: '',
                activeProduct:false,
                roundTrip: false
            }
        }
}
componentWillMount(){
        let prodCode=this.props.prodCode;

    if(this.props.newProduct){
        this.setState({
            product:this.props.prodTemplate,
            newProduct:true,
            saveFlag:'new'
        });
        // console.log(this.state.product);
        // console.log("passed",this.props.prodTemplate);

    }
    else{
        let prodUrl=apiUrl.prodCode+prodCode;
        Request.get(prodUrl).accept('json').then(response=>{ let prod=response.body;
            this.setState({
                product:prod[0],
                temp:prod
            })
        });
        // console.log(apiUrl.roomType);
    }
    // let mealPlan=[];
    let mealPlan_url=apiUrl.mealPlan;
    // Request.get(mealPlan_url).accept('json').then(response=>{mealPlan.push(response.body.map(plans=>{return {"val":plans.mealPlanId,"name":plans.mealPlan}}));this.setState({mealPlan:mealPlan[0]})});
    Request.get(mealPlan_url).accept('json').end((err,res)=>{
            if(err || !res.ok){
                alert("Error getting MealPlan "+err);
            }else{
                if(res.body.success){
                    this.setState({
                        mealPlan:res.body.response,
                    })
                }else {
                    alert("Error getting MealPlan "+res.body.error)
                }
            }
        }
    );


    let roomType=[];
    let roomType_url=apiUrl.roomType;
    Request.get(roomType_url).accept('json').then(response=>{roomType.push(response.body.map(rooms=>{return {"val":rooms.roomTypeId,"name":rooms.roomType}}));this.setState({roomType:roomType[0]})});


    let allotCode=[];
    let allotCode_url=apiUrl.allotCode;
    Request.get(allotCode_url).accept('json').then(response=>{allotCode.push(response.body.map(allotCodes=>{return {"val":allotCodes,"name":allotCodes}}));this.setState({allotCode:allotCode[0]})});

    // let vendorList=[];
    let vendorList_url=apiUrl.vendor;
    // Request.get(vendorList_url).accept('json').then(response=>{vendorList.push(response.body.map(vendors=>{return {"val":vendors.vendorID,"name":vendors.vendorName}}));this.setState({vendorList:vendorList[0]})});
    Request.get(vendorList_url).accept('json').end((err,res)=>{
            if(err || !res.ok){
                alert("Oops"+err);
            }else{

                if(res.body.success){
                    let tmp=JSON.parse(res.body.response);
                    this.setState({
                        vendorList:tmp,
                    })

                }

            }
        }
    );

}

//     getAllotCodes(){
//         let allotCode=[];
//         let allotCode_url=apiUrl.allotCode;
//         Request.get(allotCode_url).accept('json').then(response=>{allotCode.push(response.body.map(allotCodes=>{return {"val":allotCodes,"name":allotCodes}}))});
//         return allotCode[0];
//     }
// static allotCodeOptions=this.getAllotCodes;
//
//
// static vendorListOptions=this.getVendorList;
//     getVendorList(){
//     let vendorList=[];
//     let vendorList_url=apiUrl.vendor;
//     Request.get(vendorList_url).accept('json').then(response=>{vendorList.push(response.body.map(vendors=>{return {"val":vendors.vendorID,"name":vendors.vendorName}}))});
//     return vendorList[0];
// }

    roomType(val){
        let tmp={...this.state.product};
        tmp.roomType.roomTypeId=val;
        tmp.roomType.roomType=this.getName(this.state.roomType,val);
        this.setState({
            product:tmp
        });

    }

mealPlan(val){
    let tmp={...this.state.product};
    tmp.mealPlan.mealPlanID=val;
    // tmp.mealPlan.mealPlan=this.getName(this.state.mealPlan,val);
    this.setState({
        product:tmp
    });
}

vendor(val){
    let tmp={...this.state.product};
    tmp.vendor.vendorID=val;
    this.setState({
        product:tmp
    });
}

    allotCode(val){
        let tmp={...this.state.product};
        tmp.allotCode=val;
        this.setState({
            product:tmp
        });
    }


displayPriority(val){
    let tmp={...this.state.product};
    tmp.webPriority=val;
    this.setState({
        product:tmp
    });
}
static priorityOptions=[{val:"1",name:'Perm Room'},{val:"2",name:'Priority Regular'},{val:"3",name:'Regular'},{val:"4",name:'Elite'},{val:"5",name:'Team Player'}];

displayCategory(str){
   let tmp={...this.state.product};
    tmp.displayCategory=str;
        this.setState({
            product:tmp
        });
}
static displayCategoryOptions=[{val:'*',name:'*'},{val:'**',name:'**'},{val:'***',name:'***'},{val:'****',name:'****'},{val:'*****',name:'*****'}];

confirmationType(val){
    let tmp={...this.state.product};
    tmp.productConfiguration.confType=val;
    this.setState({
        product:tmp
    });
    }

static confOptions =[{val:1,name:1},{val:2,name:2},{val:3,name:3}];

static dummyArray= [{val:'*',name:'*'},{val:'**',name:'**'},{val:'***',name:'***'},{val:'****',name:'****'},{val:'*****',name:'*****'}];



handleSave(flag){
    this.props.save(this.state.product,flag);
}



dateFormat(date){
            let tempObj=moment(date);
            let tempDate=tempObj.get('year')+'-'+tempObj.get('month')+1+'-'+tempObj.get('date');
            return tempDate;
    // console.log(tempDate);
    }
getDefaultSelect(array,str){
    let obj=array.find((row)=>{return row.val===str});
    // console.log(obj.name);
    // console.log("defselect", array.find((row)=>{return row.val===str}))
    if(obj!==undefined){
        return obj.name;
    }else return '';
}
getName(array,val){
    let obj=array.find((row)=>{return row.val===val});
    if(obj!==undefined){
        return obj.name;
    }else return '';
}
getDate(str){
    if (str===null){
        return null;
    }else {
        return moment(str).utc().utcOffset(360);
    }
}
getBtnStyle(state){
    return (state)?"success":"danger";
}
getBtnText(state){
    return (state)?"Changeable":"Restricted";
}
// cloningMessage(){
//     this.setState({saveFlag:"new",cloneModal:true});
//     setTimeout(this.setState({cloneModal:false}),3000);
// }
    render(){
        let cloneMsg=<Modal show={this.state.cloneModal} onHide={()=>{this.setState({cloneModal:false})}}>
            <Modal.Header closeButton><Modal.Title>Product Cloning</Modal.Title></Modal.Header>
            <Modal.Body>
                <ControlLabel><FormControl type={"text"} placeholder={"Enter new product code"} onChange={(e)=>{let tmp={...this.state.product};tmp.prodCode=e.target.value;this.setState({product:tmp})}}/></ControlLabel>
                <FormGroup>
                    <Checkbox checked={this.state.copyFlags.copyAllotment} onChange={()=>{let tmp={...this.state.copyFlags};tmp.copyAllotment=!tmp.copyAllotment;this.setState({copyFlags:tmp})}}> Copy Allotment</Checkbox>
                    <Checkbox checked={this.state.copyFlags.copyDefaultCancellation} onChange={()=>{let tmp={...this.state.copyFlags};tmp.copyDefaultCancellation=!tmp.copyDefaultCancellation;this.setState({copyFlags:tmp})}}>Copy Default Cancellation</Checkbox>
                    <Checkbox checked={this.state.copyFlags.copyPricing} onChange={()=>{let tmp={...this.state.copyFlags};tmp.copyPricing=!tmp.copyPricing;this.setState({copyFlags:tmp})}}>Copy Pricing</Checkbox>
                    <Checkbox checked={this.state.copyFlags.copyWebAccess} onChange={()=>{let tmp={...this.state.copyFlags};tmp.copyWebAccess=!tmp.copyWebAccess;this.setState({copyFlags:tmp})}}>Copy WebAccess</Checkbox>
                </FormGroup>
            </Modal.Body>
            <Modal.Footer><Button bsStyle={"success"} onClick={()=>{this.setState({cloneModal:false})}}>Continue</Button></Modal.Footer>
        </Modal>;
        let toolBartop=<Col md={10} mdOffset={1}> <ButtonToolbar>
            <Button bsStyle={(this.state.product.webAccess==="DENY_ALL")?"danger":"warning"} onClick={()=>{let url=`http://dev2.volateam.com/buddin/res/web_access.php?action=none&PkgCode=`+this.state.product.prodCode;window.open(url, 'win_default', 'width=925,height=800,resizeable=yes,screenX=40,screenY=60', 'true')}}>Product Web Access</Button>
            <Button onClick={()=>{let url=`http://dev2.volateam.com/buddin/res/cxl_penalty.php?action=none&PkgCode=`+this.state.product.prodCode;window.open(url, 'win_default', 'width=925,height=800,resizeable=yes,screenX=40,screenY=60', 'true')}}>Default Cxl Penalty</Button>
            <Button onClick={()=>{let url=`http://dev2.volateam.com/buddin/res/product_combo_manager.php?init=BU&username=buddin&access=2&prodecode=`+this.state.product.prodCode+`&vendorid=`+this.state.product.vendor.vendorID;window.open(url, 'win_default', 'width=925,height=800,resizeable=yes,screenX=40,screenY=60', 'true')}}>Add Combo</Button>
            <Button onClick={()=>{let url=`http://dev2.volateam.com/buddin/res/price.php?calledby=product&init=BU&username=buddin&access=2&ProdCode=`+this.state.product.prodCode;window.open(url, 'win_default', 'width=925,height=800,resizeable=yes,screenX=40,screenY=60', 'true')}}>Standard Pricing</Button>
            <Button onClick={()=>{let url=`http://dev2.volateam.com/buddin/res/allot_info.php?init=BU&username=buddin&access=2&ProdCode=`+this.state.product.prodCode;window.open(url, 'win_default', 'width=925,height=800,resizeable=yes,screenX=40,screenY=60', 'true')}}>Allotment</Button>
            <Button>History</Button>
            <Button>Publish to DREAM</Button>
        </ButtonToolbar> </Col>;
        let toolBarBottom=<Col md={10} mdOffset={2}><br/><ButtonToolbar>
            <Button bsStyle={"success"} onClick={()=>{this.handleSave(this.state.saveFlag)}}>Save</Button>
            <Button disabled>Delete Product</Button>
            <Button>Refresh Data</Button>
            <Button bsStyle={"info"} onClick={()=>{this.setState({cloneModal:true})}}>Clone</Button>
            <Button>Special Agency Program</Button>
            <Button>Descriptions & Photos"</Button>

        </ButtonToolbar><br/><br/></Col>;
        let formPt1=
            <FormGroup >
                <Col md={10} mdOffset={1}>
                    <ControlLabel>Description:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.description} onChange={(e)=>{let tmp={...this.state.product};tmp.description=e.target.value;this.setState({product:tmp})}}/>
                    <ControlLabel>Product Description:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.productDescription} onChange={(e)=>{let tmp={...this.state.product};tmp.productDescription=e.target.value;this.setState({product:tmp})}}/>

                </Col>

                <Col md={6}>
                    <Checkbox checked={this.state.product.productFlags.flightItinFlag}  onChange={()=>{let tmp={...this.state.product};tmp.productFlags.flightItinFlag=!tmp.productFlags.flightItinFlag;this.setState({product:tmp})}}>Include Item Comment</Checkbox>
                    <ControlLabel>Comment 1:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.productFlightInfo.flight1} onChange={(e)=>{let tmp={...this.state.product};tmp.productFlightInfo.flight1=e.target.value;this.setState({product:tmp})}}/>
                    <ControlLabel>Comment 2:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.productFlightInfo.flight2} onChange={(e)=>{let tmp={...this.state.product};tmp.productFlightInfo.flight2=e.target.value;this.setState({product:tmp})}}/>
                </Col>
                <Col md={6}>
                    <ControlLabel>Comment 3:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.productFlightInfo.flight3} onChange={(e)=>{let tmp={...this.state.product};tmp.productFlightInfo.flight3=e.target.value;this.setState({product:tmp})}}/>
                    <ControlLabel>Comment 4:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.productFlightInfo.flight4} onChange={(e)=>{let tmp={...this.state.product};tmp.productFlightInfo.flight4=e.target.value;this.setState({product:tmp})}}/>
                </Col>
                <Col md={12}>
                    <ControlLabel>Voucher Instructions:</ControlLabel>
                    <Checkbox checked={(this.state.product.productInstructions.instr1==='standard')} onChange={()=>{let tmp={...this.state.product};tmp.productInstructions.instr1=(this.state.product.productInstructions.instr1==='standard')?'':'standard';this.setState({product:tmp})}}>HOTELS ONLY, Check here to include standard hotel instructions. (for example: "Please provide 2 4-person rooms for 3 nights;
                    arrival date: 12/15/2017. Departure date: 12/18/2017.")</Checkbox>
                </Col>
                <Col md={6}>
                    <ControlLabel>Instr 2:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.productInstructions.instr2} onChange={(e)=>{let tmp={...this.state.product};tmp.productInstructions.instr2=e.target.value;this.setState({product:tmp})}}/>
                    <ControlLabel>Instr 3:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.productInstructions.instr3} onChange={(e)=>{let tmp={...this.state.product};tmp.productInstructions.instr3=e.target.value;this.setState({product:tmp})}}/>
                </Col>
                <Col md={6}>
                    <ControlLabel>Instr 4:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.productInstructions.instr4} onChange={(e)=>{let tmp={...this.state.product};tmp.productInstructions.instr4=e.target.value;this.setState({product:tmp})}}/>
                    <ControlLabel>Instr 5:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.productInstructions.instr5} onChange={(e)=>{let tmp={...this.state.product};tmp.productInstructions.instr5=e.target.value;this.setState({product:tmp})}}/>
                </Col>
            </FormGroup>;
        let formPt2=<FormGroup>
            <Col md={4}>
                <ControlLabel>Department type:</ControlLabel>
                <FormGroup>
                    <Radio name={"dept"} checked={(this.state.product.dept.label === "FIT")} onChange={()=>{let tmp={...this.state.product};tmp.dept.id=0;tmp.dept.label="FIT";this.setState({product:tmp})}} inline>FIT</Radio>
                    <Radio name={"dept"} checked={(this.state.product.dept.label === "GROUP")} onChange={()=>{let tmp={...this.state.product};tmp.dept.id=1;tmp.dept.label="GROUP";this.setState({product:tmp})}} inline>GROUP</Radio>
                </FormGroup>
                <Checkbox checked={this.state.product.permRoomCode} onChange={()=>{let tmp={...this.state.product};tmp.permRoomCode=!tmp.permRoomCode;this.setState({product:tmp})}}> Is A Perm Room Code</Checkbox>
                <Checkbox checked={this.state.product.specialRoomFlag} onChange={()=>{let tmp={...this.state.product};tmp.specialRoomFlag=!tmp.specialRoomFlag;this.setState({product:tmp})}}>Is a Special Room Code</Checkbox>
                <Checkbox checked={this.state.product.productFlags.pULocFlag} onChange={()=>{let tmp={...this.state.product};tmp.productFlags.pULocFlag=!tmp.productFlags.pULocFlag;this.setState({product:tmp})}}>Display Pickup Location</Checkbox>
                <Checkbox checked={this.state.product.productFlags.pUTimeFlag} onChange={()=>{let tmp={...this.state.product};tmp.productFlags.pUTimeFlag=!tmp.productFlags.pUTimeFlag;this.setState({product:tmp})}} >Display Pickup Time</Checkbox>
                <Checkbox checked={this.state.product.productFlags.misc1Flag} onChange={()=>{let tmp={...this.state.product};tmp.productFlags.misc1Flag=!tmp.productFlags.misc1Flag;this.setState({product:tmp})}}>Misc1Flag</Checkbox>
                <Checkbox checked={this.state.product.productFlags.misc2Flag} onChange={()=>{let tmp={...this.state.product};tmp.productFlags.misc2Flag=!tmp.productFlags.misc2Flag;this.setState({product:tmp})}}>Misc2Flag</Checkbox>
                <Checkbox checked={this.state.product.productConfiguration.hotel} onChange={()=>{let tmp={...this.state.product};tmp.productConfiguration.hotel=!tmp.productConfiguration.hotel;this.setState({product:tmp})}}>Is Hotel</Checkbox>
                <Checkbox checked={this.state.product.restrictionProperties.b2cRestricted} onChange={()=>{let tmp={...this.state.product};tmp.restrictionProperties.b2cRestricted=!tmp.restrictionProperties.b2cRestricted;this.setState({product:tmp})}}>B2C restricted</Checkbox>
                <Checkbox checked={this.state.product.promotionalDetails.usesLanguage} onChange={()=>{let tmp={...this.state.product};tmp.promotionalDetails.usesLanguage=!tmp.promotionalDetails.usesLanguage;this.setState({product:tmp})}}>Uses Language</Checkbox>
                <br/>
                <br/>
                <br/>
                <br/>
            </Col>
            <Col md={4}>

                {/*need work after this*/}
                <Checkbox checked={this.state.product.priceProperties.prodCom} onChange={()=>{let tmp={...this.state.product};tmp.priceProperties.prodCom=!tmp.priceProperties.prodCom;this.setState({product:tmp})}}>Commissionable</Checkbox>
                <Checkbox checked={this.state.product.productFlags.allotmentFlag} onChange={()=>{let tmp={...this.state.product};tmp.productFlags.allotmentFlag=!tmp.productFlags.allotmentFlag;this.setState({product:tmp})}}>Uses Allotment</Checkbox>
                <ControlLabel>Group Pricing Type:</ControlLabel>
                <FormGroup>
                    <Radio name={"priceType"} checked={(this.state.product.groupPricingType === 'FIXED')} onChange={()=>{let tmp={...this.state.product};tmp.groupPricingType='FIXED';this.setState({product:tmp})}} inline>FIXED</Radio>
                    <Radio name={"priceType"} checked={(this.state.product.groupPricingType === 'VARIABLE')} onChange={()=>{let tmp={...this.state.product};tmp.groupPricingType='VARIABLE';this.setState({product:tmp})}} inline>VARIABLE</Radio>
                </FormGroup>
                <Checkbox checked={this.state.product.promotionalDetails.brochureData} onChange={()=>{let tmp={...this.state.product};tmp.promotionalDetails.brochureData=!tmp.promotionalDetails.brochureData;this.setState({product:tmp})}}>Include in Elite Brochure Data</Checkbox>
                <Checkbox checked={this.state.product.promotionalDetails.eliteBrochureData} onChange={()=>{let tmp={...this.state.product};tmp.promotionalDetails.eliteBrochureData=!tmp.promotionalDetails.eliteBrochureData;this.setState({product:tmp})}}>Include in Brochure Data</Checkbox>
                <Checkbox checked={this.state.product.extranet} onChange={()=>{let tmp={...this.state.product};tmp.extranet=!tmp.extranet;this.setState({product:tmp})}}>Exclude from Extranet Agency</Checkbox>
                <Checkbox checked={this.state.product.productFlags.packageFlag} onChange={()=>{let tmp={...this.state.product};tmp.productFlags.packageFlag=!tmp.productFlags.packageFlag;this.setState({product:tmp})}}>This is a Package</Checkbox>
            </Col>
            <Col md={4}>
                <Checkbox checked={this.state.product.priceProperties.nonRefundable} onChange={()=>{let tmp={...this.state.product};tmp.priceProperties.nonRefundable=!tmp.priceProperties.nonRefundable;this.setState({product:tmp})}}>Non Refundable</Checkbox>
                <Checkbox checked={this.state.product.productConfiguration.markTUIGermany} onChange={()=>{let tmp={...this.state.product};tmp.productConfiguration.markTUIGermany=!tmp.productConfiguration.markTUIGermany;this.setState({product:tmp})}}>Map for TUI Germany</Checkbox>
                <Checkbox checked={this.state.product.productConfiguration.markTUISpain} onChange={()=>{let tmp={...this.state.product};tmp.productConfiguration.markTUISpain=!tmp.productConfiguration.markTUISpain;this.setState({product:tmp})}}>Map for TUI Spain</Checkbox>
                {/*couldn't find value*/}<Checkbox  onChange={()=>{let tmp={...this.state.product};;this.setState({product:tmp})}}>Map for TUI UK</Checkbox>
                <ControlLabel>Confirmation Type</ControlLabel>
                <SelectList getVal={this.confirmationType.bind(this)} options={ProductInfo.confOptions} default={this.state.product.productConfiguration.confType} />
                <Checkbox checked={this.state.product.productFlags.priceOverrideFlag} onChange={()=>{let tmp={...this.state.product};tmp.productFlags.priceOverrideFlag=!tmp.productFlags.priceOverrideFlag;this.setState({product:tmp})}}>Res Agents May Override Price</Checkbox>
                <Checkbox checked={this.state.product.productFlags.displayOccFlag} onChange={()=>{let tmp={...this.state.product};tmp.productFlags.displayOccFlag=!tmp.productFlags.displayOccFlag;this.setState({product:tmp})}}>Display Occupancy Info</Checkbox>
                <Checkbox checked={this.state.product.productFlags.displayNightsFlag} onChange={()=>{let tmp={...this.state.product};tmp.productFlags.displayNightsFlag=!tmp.productFlags.displayNightsFlag;this.setState({product:tmp})}}>Display Night Info</Checkbox>
            </Col>

            <Col md={10} mdOffset={2}>
                <ButtonToolbar>
                    <Button onClick={()=>{this.setState({restrictChange:!this.state.restrictChange})}}>Restrict Change</Button>
                    <Button>Package Info</Button>
                    <Button>Suggested Items</Button>
                </ButtonToolbar><br/>
            </Col>
        </FormGroup>;
        let formPt3=<FormGroup><Col md={12}>
            <Col md={6}>

                <ControlLabel>Max Occ:</ControlLabel>
                <FormControl value={this.state.product.occupancyConfiguration.maxOcc} onChange={(e)=>{let tmp={...this.state.product};tmp.occupancyConfiguration.maxOcc=e.target.value;this.setState({product:tmp})}}/>
                <ControlLabel>Category</ControlLabel>
                <SelectList options={ProductInfo.displayCategoryOptions} getVal={this.displayCategory.bind(this)} default={this.getDefaultSelect(ProductInfo.displayCategoryOptions,this.state.product.displayCategory)}/>

                <ControlLabel>Allotment Code:</ControlLabel>
                <SelectList options={this.state.allotCode} etVal={this.allotCode.bind(this)} default={this.state.product.allotCode}/>

                <ControlLabel>Room Type:</ControlLabel>
                <SelectList options={this.state.roomType} getVal={this.roomType.bind(this)} default={this.state.product.roomType.roomType}/>

                <ControlLabel>Vendor:</ControlLabel>
                <Select searchable={true} clearable={true} placeholder={this.state.product.vendor.vendorName} resetValue={"select"} value={this.state.product.vendor.vendorID} options={this.state.vendorList} onChange={(select)=>{let tmp={...this.state.product};tmp.vendor.vendorID=select.value;tmp.vendor.vendorName=select.label;this.setState({product:tmp})}}/>
                {/*<SelectList options={this.state.vendorList} getVal={this.vendor.bind(this)} default={this.state.product.vendor.vendorName}/>*/}

                <ControlLabel>Meal Plan</ControlLabel>
                <Select clearable={true} resetValue={"select"} searchable={true} value={this.state.product.mealPlan.mealPlanID} onChange={(select)=>{let tmp={...this.state.product};tmp.mealPlan.mealPlanID=select.value;tmp.mealPlan.mealPlan=select.label;this.setState({product:tmp})}} options={this.state.mealPlan} />
                {/*<SelectList options={this.state.mealPlan} getVal={this.mealPlan.bind(this)} default={this.getDefaultSelect(this.state.mealPlan,this.state.product.mealPlan.mealPlanID)}/>*/}


                <ControlLabel>Web Display Priority</ControlLabel>
                <SelectList getVal={this.displayPriority.bind(this)} options={ProductInfo.priorityOptions} default={this.getDefaultSelect(ProductInfo.priorityOptions,this.state.product.webPriority)}/>
                <ControlLabel>Web Priority</ControlLabel>
                {/*<FormGroup>*/}
                    <Col >starts:<DatePicker selected={this.getDate(this.state.product.webDuration.startDate)} dateFormat={"YYYY-MM-DD"} onChange={(date)=>{let tmp={...this.state.product};tmp.webDuration.startDate=this.dateFormat(date);this.setState({product:tmp})}}/></Col>
                    <Col > ends:<DatePicker selected={this.getDate(this.state.product.webDuration.endDate)} dateFormat={"YYYY-MM-DD"} onChange={(date)=>{let tmp={...this.state.product};tmp.webDuration.endDate=this.dateFormat(date);this.setState({product:tmp})}}/></Col>
                {/*</FormGroup>*/}
                <br/>
                <br/>
                <br/>
                <br/>
            </Col>
            <Col md={6}>

                <ControlLabel>Calender Prority</ControlLabel>
                <FormControl type={"text"} value={this.state.product.calendarPriority} onChange={(e)=>{let tmp={...this.state.product};tmp.calendarPriority=e.target.value;this.setState({product:tmp})}}/>
                <ControlLabel>Default Number of Nights:</ControlLabel>
                <FormControl type={"text"} value={this.state.product.occupancyConfiguration.defaultNights} onChange={(e)=>{let tmp={...this.state.product};tmp.occupancyConfiguration.defaultNights=e.target.value;this.setState({product:tmp})}}/>
                <ControlLabel>Min Number of Nights:</ControlLabel>
                <FormControl type={"text"} value={this.state.product.occupancyConfiguration.minNights} onChange={(e)=>{let tmp={...this.state.product};tmp.occupancyConfiguration.minNights=e.target.value;this.setState({product:tmp})}}/>
                <ControlLabel>Notify when booked to:</ControlLabel>
                <FormControl type={"text"} value={this.state.product.notifyEmails} onChange={(e)=>{let tmp={...this.state.product};tmp.notifyEmails=e.target.value;this.setState({product:tmp})}}/>
                <ControlLabel>Notify Date</ControlLabel>
                    {/*<FormGroup>*/}
                        <Col >starts:<DatePicker selected={this.getDate(this.state.product.notifyDuration.startDate)} dateFormat={"YYYY-MM-DD"} onChange={(date)=>{let tmp={...this.state.product};tmp.notifyDuration.startDate=this.dateFormat(date);this.setState({product:tmp})}}/></Col>
                        <Col > ends:<DatePicker selected={this.getDate(this.state.product.notifyDuration.endDate)} dateFormat={"YYYY-MM-DD"} onChange={(date)=>{let tmp={...this.state.product};tmp.notifyDuration.endDate=this.dateFormat(date);this.setState({product:tmp})}}/></Col>
                    {/*</FormGroup>*/}
                <ControlLabel>Addl Net Product Charge:</ControlLabel>
                <FormControl type={"text"} value={this.state.product.priceProperties.productCharge} onChange={(e)=>{let tmp={...this.state.product};tmp.priceProperties.productCharge=e.target.value;this.setState({product:tmp})}}/>
                <Checkbox checked={this.state.product.promotionalDetails.freeSellProgram} onChange={()=>{let tmp={...this.state.product};tmp.promotionalDetails.freeSellProgram=!tmp.promotionalDetails.freeSellProgram;this.setState({product:tmp})}}>Free Sell Program</Checkbox>
                <ControlLabel>Free Sell Notes:</ControlLabel>
                <FormControl type={"text"} value={this.state.product.promotionalDetails.freeSellNotes} onChange={(e)=>{let tmp={...this.state.product};tmp.promotionalDetails.freeSellNotes=e.target.value;this.setState({product:tmp})}}/>
            </Col>
            {/*{console.log(this.state.product.disneyInformation.disneyPackages[0].ordinal)}*/}
        </Col>
        </FormGroup>;
        let formPt4=<FormGroup>
            <Col md={8} mdOffset={2}>
                <b>Disney</b>
                <ControlLabel>Disney Hotel Code</ControlLabel>
                <FormControl type={"text"} value={this.state.product.disneyInformation.disneyHotelCode} onChange={(e)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyHotelCode=e.target.value;this.setState({product:tmp})}}/>
                <ControlLabel>Disney Room Type</ControlLabel>
                <FormControl type={"text"} value={this.state.product.disneyInformation.disneyRoomType} onChange={(e)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyRoomType=e.target.value;this.setState({product:tmp})}}/>
            </Col>
            <Col md={6}>


                    <ControlLabel>Disney Hotel/Ticket Year 1:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.disneyInformation.disneyCodeCurrentYear} onChange={(e)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyCodeCurrentYear=e.target.value;this.setState({product:tmp})}}/>
                    <ControlLabel>Disney Package Code:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.disneyInformation.disneyPackages[0].disneyPackageCode} onChange={(e)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyPackages[0].disneyPackageCode=e.target.value;this.setState({product:tmp})}}/>
                    {/*<FormGroup>*/}
                        <Col>Start Date 1:<DatePicker selected={this.getDate(this.state.product.disneyInformation.disneyPackages[0].disneyCodeDuration.startDate)} dateFormat={"YYYY-MM-DD"} onChange={(date)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyPackages[0].disneyCodeDuration.startDate=this.dateFormat(date);this.setState({product:tmp})}}/></Col>
                        <Col> End Date 1:<DatePicker selected={this.getDate(this.state.product.disneyInformation.disneyPackages[0].disneyCodeDuration.endDate)} dateFormat={"YYYY-MM-DD"} onChange={(date)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyPackages[0].disneyCodeDuration.endDate=this.dateFormat(date);this.setState({product:tmp})}}/></Col>
                    {/*</FormGroup>*/}
                    <ControlLabel>Adult Ticket Code:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.disneyInformation.disneyPackages[0].disneyAdultTicketCode} onChange={(e)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyPackages[0].disneyAdultTicketCode=e.target.value;this.setState({product:tmp})}}/>
                    <ControlLabel>Child Ticket Code:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.disneyInformation.disneyPackages[0].disneyChildTicketCode} onChange={(e)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyPackages[0].disneyChildTicketCode=e.target.value;this.setState({product:tmp})}}/>


            </Col>
            <Col md={6} >


                    <ControlLabel>***Disney Hotel/Ticket Year 2:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.disneyInformation.disneyCodeNextYear} onChange={(e)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyCodeNextYear=e.target.value;this.setState({product:tmp})}}/>
                    <ControlLabel>Disney Package Code:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.disneyInformation.disneyPackages[1].disneyPackageCode} onChange={(e)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyPackages[1].disneyPackageCode=e.target.value;this.setState({product:tmp})}}/>
                    {/*<FormGroup>*/}
                        <Col>Start Date 2:<DatePicker selected={this.getDate(this.state.product.disneyInformation.disneyPackages[1].disneyCodeDuration.startDate)} dateFormat={"YYYY-MM-DD"} onChange={(date)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyPackages[1].disneyCodeDuration.startDate=this.dateFormat(date);this.setState({product:tmp})}}/></Col>
                        <Col> End Date 2:<DatePicker selected={this.getDate(this.state.product.disneyInformation.disneyPackages[1].disneyCodeDuration.endDate)} dateFormat={"YYYY-MM-DD"} onChange={(date)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyPackages[1].disneyCodeDuration.endDate=this.dateFormat(date);this.setState({product:tmp})}}/></Col>
                    {/*</FormGroup>*/}
                    <ControlLabel>Adult Ticket Code:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.disneyInformation.disneyPackages[1].disneyAdultTicketCode} onChange={(e)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyPackages[1].disneyAdultTicketCode=e.target.value;this.setState({product:tmp})}}/>
                    <ControlLabel>Child Ticket Code:</ControlLabel>
                    <FormControl type={"text"} value={this.state.product.disneyInformation.disneyPackages[1].disneyChildTicketCode} onChange={(e)=>{let tmp={...this.state.product};tmp.disneyInformation.disneyPackages[1].disneyChildTicketCode=e.target.value;this.setState({product:tmp})}}/>


            </Col>
            <Col md={6}>
            <ControlLabel>Block Report</ControlLabel>

                <Radio>Do not include in Block Reports</Radio>
                <Radio>Include in Standard Block Report</Radio>
                <Radio checked={this.state.product.disneyInformation.reportDisneyFlag} onChange={()=>{let tmp={...this.state.product};tmp.disneyInformation.reportDisneyFlag=!tmp.disneyInformation.reportDisneyFlag;this.setState({product:tmp})}}>Include in Disney Room Report for ABS</Radio>
                <Radio checked={this.state.product.disneyInformation.reportDisneyTicketFlag} onChange={()=>{let tmp={...this.state.product};tmp.disneyInformation.reportDisneyTicketFlag=!tmp.disneyInformation.reportDisneyTicketFlag;this.setState({product:tmp})}}>Include in Disney Ticket Report for ABS</Radio>

            </Col>
            <Col md={6}>
                <ControlLabel>Resort Fee: $</ControlLabel>
                <FormControl/>
                <ControlLabel>Resort Fee Type:</ControlLabel>
                <SelectList options={ProductInfo.dummyArray}/>

            </Col>

        </FormGroup>;

        let productInfo=<Form horizontal>
            {/*<Tabs id={"tabbed-form"} activeKey={this.state.tabSelected} onSelect={(key)=>{this.setState({tabSelected:key})}}>*/}
                {/*<Tab eventKey={1} title={"Part 1"}>{formPt1}</Tab>*/}
                {/*<Tab eventKey={2} title={"Part 2"}>{formPt2}</Tab>*/}
                {/*<Tab eventKey={3} title={"Part 3"}>{formPt3}</Tab>*/}
                {/*<Tab eventKey={4} title={"Part 4"}>{formPt4}</Tab>*/}
            {/*</Tabs>*/}

            <Grid>
                <Row className={"prodInfo"}>
                    <Col md={6} className={"prodInfoParts"}>{formPt1}</Col>
                    <Col md={6} className={"prodInfoParts"}>{formPt2}</Col>
                </Row>
                <Row  className={"prodInfo"}>
                    <Col md={6} className={"prodInfoParts"}>{formPt3}</Col>
                    <Col md={6} className={"prodInfoParts"}>{formPt4}</Col>
                </Row>
            </Grid>


        </Form>;
        const restrictChange=<div>
            <Modal show={this.state.restrictChange} onHide={()=>{this.setState({restrictChange:!this.state.restrictChange})}}>
                        <Modal.Header closeButton><Modal.Title className={"text-center"}>Change Restriction for <b>{this.state.product.prodCode}</b></Modal.Title></Modal.Header>
                        <Modal.Body >

                                <table className={"text-center"}>
                                    <tr>
                                        <th>Change Options</th>
                                        <th>Restriction</th>
                                    </tr>

                                    <tr><td>Date</td><td><Button onClick={()=>{let tmp={...this.state.product};tmp.restrictionProperties.dateChangeRestricted=!tmp.restrictionProperties.dateChangeRestricted;this.setState({product:tmp})}} bsStyle={this.getBtnStyle(this.state.product.restrictionProperties.dateChangeRestricted)}>{this.getBtnText(this.state.product.restrictionProperties.dateChangeRestricted)}</Button></td></tr>
                                    <tr><td>Occ</td><td><Button onClick={()=>{let tmp={...this.state.product};tmp.restrictionProperties.occChangeRestricted=!tmp.restrictionProperties.occChangeRestricted;this.setState({product:tmp})}} bsStyle={this.getBtnStyle(this.state.product.restrictionProperties.occChangeRestricted)}>{this.getBtnText(this.state.product.restrictionProperties.occChangeRestricted)}</Button></td></tr>
                                    <tr><td>Nights</td><td><Button onClick={()=>{let tmp={...this.state.product};tmp.restrictionProperties.nightsChangeRestricted=!tmp.restrictionProperties.nightsChangeRestricted;this.setState({product:tmp})}} bsStyle={this.getBtnStyle(this.state.product.restrictionProperties.nightsChangeRestricted)}>{this.getBtnText(this.state.product.restrictionProperties.nightsChangeRestricted)}</Button></td></tr>
                                    <tr><td>Qty</td><td><Button onClick={()=>{let tmp={...this.state.product};tmp.restrictionProperties.qtyChangeRestricted=!tmp.restrictionProperties.qtyChangeRestricted;this.setState({product:tmp})}} bsStyle={this.getBtnStyle(this.state.product.restrictionProperties.qtyChangeRestricted)}>{this.getBtnText(this.state.product.restrictionProperties.qtyChangeRestricted)}</Button></td></tr>
                                    <tr><td>Type</td><td><Button onClick={()=>{let tmp={...this.state.product};tmp.restrictionProperties.typeChangeRestricted=!tmp.restrictionProperties.typeChangeRestricted;this.setState({product:tmp})}} bsStyle={this.getBtnStyle(this.state.product.restrictionProperties.typeChangeRestricted)}>{this.getBtnText(this.state.product.restrictionProperties.typeChangeRestricted)}</Button></td></tr>
                                    <tr><td>Price</td><td><Button onClick={()=>{let tmp={...this.state.product};tmp.restrictionProperties.priceChangeRestricted=!tmp.restrictionProperties.priceChangeRestricted;this.setState({product:tmp})}} bsStyle={this.getBtnStyle(this.state.product.restrictionProperties.priceChangeRestricted)}>{this.getBtnText(this.state.product.restrictionProperties.priceChangeRestricted)}</Button></td></tr>
                                    <tr><td>Passenger</td><td><Button onClick={()=>{let tmp={...this.state.product};tmp.restrictionProperties.paxChangeRestricted=!tmp.restrictionProperties.paxChangeRestricted;this.setState({product:tmp})}} bsStyle={this.getBtnStyle(this.state.product.restrictionProperties.paxChangeRestricted)}>{this.getBtnText(this.state.product.restrictionProperties.paxChangeRestricted)}</Button></td></tr>

                                </table>

                        </Modal.Body>
                <Modal.Footer><Button onClick={()=>{this.setState({restrictChange:!this.state.restrictChange})}} bsStyle={"primary"}>Save</Button></Modal.Footer>
            </Modal>
        </div>;
        return(
            <div className={"prodContainer"}>
                {cloneMsg}
                {restrictChange}
                <div className={"row"}>
                    {toolBartop}

                    <Col md={12}><h3 className={"text-center"}>Product Code:<b>{this.state.product.prodCode}</b> Note: this is an {(this.state.product.activeProduct)?"Active":"Inactive"} PUSH Product</h3></Col>
                </div>
                {productInfo}
                {/*{console.log('testing',dataArray.vendorListOptions)}*/}
                {toolBarBottom}

            </div>

        );
    }
}

ProductInfo.propTypes={
    prodCode:PropTypes.string,
    save:PropTypes.func,
    update:PropTypes.func,
    prodTemplate:PropTypes.object,
};