import  React from "react";
import PropTypes from 'prop-types';


export class ProductList extends React.Component{

    render(){
        let description,vendor,category,department;
        const urlParams=new URLSearchParams(window.location.search);
            if(urlParams.has('cate')){
                description=urlParams.get('description');
                vendor=urlParams.get('vendorName');
                category=urlParams.get('productCategory');
                department=urlParams.get('department');
            }
            // console.log(description,vendor);
           //  let vendor=this.props.vendorName;
           //  let description=this.props.description;
           //  let category=this.props.category;
           //  let department=this.props.department;
           return(
               <div>
                   {/*{(vendor!==undefined || description!==undefined)?console.log("vendorName: ",vendor," description: ",description," cate: ",category," dept: ",department):''}*/}
                   <div className="container">

                       <div className="starter-template">


                           <div className="table-responsive">
                               <table id="paginatedTable" className="table table-striped">
                                   <thead>
                                   <tr>
                                       <th>prodCode</th>
                                       <th>description</th>
                                       <th>vendorId</th>
                                       <th>productCategory</th>
                                       <th>department</th>
                                   </tr>
                                   </thead>

                                   <tfoot>
                                   <tr>
                                       <th>prodCode</th>
                                       <th>description</th>
                                       <th>vendorName</th>
                                       <th>productCategory1</th>
                                       <th>department1</th>
                                   </tr>
                                   </tfoot>


                               </table>
                           </div>


                       </div>

                   </div>

               </div>
           );

    }
}

ProductList.propTypes={


};
