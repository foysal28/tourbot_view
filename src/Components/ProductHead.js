import React from "react";
import {FormGroup, Button, Col, FormControl, Form, Radio, Glyphicon, ButtonToolbar, Modal} from 'react-bootstrap';


export class ProductHead extends React.Component{
    constructor(){
        super();
        this.state={
            description:'',
            vendorName:'',
            codeValid:null,
            desValid:null,
            venValid:null,
            prodCodeValid:null,
            btn1enable:true,
            btn2enable:true,
            btn3enable:true,
            btn4enable:true,
            categoryOptions:[],
            deptOptions:[],
            category:'',
            department:'',
            newProdModal:false,
        };
    }

    validProdCode(prodCode){
        let len=prodCode.length;
        if(len<7 && len!==0){
            this.setState({codeValid:"warning",btn1enable:true});
        }
        else if(len>=7){
            this.setState({codeValid:"success",btn1enable:false});
        }
        else {this.setState({codeValid:null,btn1enable:true});
        }
    }
    validDesc(description){
        let len=description.length;
        if(len>3){
            this.setState({btn2enable:false,desValid:'success',description:this.byDes.value});
        }else if(len<4 && len!==0){
            this.setState({btn2enable:true,desValid:'warning'});
        }else {this.setState({btn2enable:true,desValid:null})}

    }
    validVendor(description){
        let len=description.length;
        if(len>3){
            this.setState({btn3enable:false,venValid:'success',vendorName:this.byVen.value});
        }else if(len<4 && len!==0){
            this.setState({btn3enable:true,venValid:'warning'});
        }else {this.setState({btn3enable:true,venValid:null})}

    }
    validNewProdCode(prodCode){
        let len=prodCode.length;
        if(len>=6 && len<11){
            this.setState({
                btn4enable: false,
                prodCodeValid:'success'
            })
        }else if(len<6 && len!==0){
            this.setState({
                btn4enable: true,
                prodCodeValid:'warning'
            })
        }else if(len>10){
            this.setState({
                btn4enable: true,
                prodCodeValid:'error'
            })
        }
        else{
            this.setState({
                btn4enable:true,
                prodCodeValid:null
            })
        }
    }

    // handleBtnDesc(props){
    //     let desc=this.state.description;
    //     let category=this.state.category;
    //     let dept=this.state.department
    // props.byDesc(desc,category,dept);
    // }
    handleCategory(id){
        this.setState({
            category:id,
        })
    }
    getProductUrl(){
        let prodCode=(this.byCode!==undefined)?this.byCode.value:'';
        if(prodCode!==''){
            return "/prodCode="+prodCode;
        }else {
            return "/products"
        }
    }
    getFilterProductsUrl(chk){
        let url=``;
        let desc=this.state.description;
        let vend=this.state.vendorName;
        let cate=this.state.category;
        let dept=this.state.department;

        // return `/products?des=`+desc+`&vend=`+vend+`&cate=`+cate+`&dept=`+dept;

        // return `/products_list&des=`+desc+`&vend=`+vend+`&cate=`+this.state.category+`&dept=`+this.state.department;
        switch (chk){
            case 'desc':
                url= `/products?description=`+desc+`&productCategory=`+cate+`&department=`+dept;
                break;
            case 'vend':
                url= `/products?vendorName=`+vend+`&productCategory=`+cate+`&department=`+dept;
                break;
        }
        return url;
    }
    addNewProd(){
        this.setState({
           newProdModal:false
        });
        window.location.href=`/newProduct=`+this.newProd.value;
    }
    componentWillMount(){
        // let tmp=[];
        // Request.get(apiUrl.listCategory).accept('json').then(response=>{tmp.push(response.body.map(categories=>{return {"val":categories.id,"name":categories.label}}));this.setState({categoryOptions:tmp[0]})});


    }
    render(){
        const newProdCode=<Modal show={this.state.newProdModal} onHide={()=>{this.setState({newProdModal:!this.state.newProdModal})}}>
            <Modal.Header closeButton>
                <Modal.Title>Add new Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>

                <FormGroup controlId={"formValidationWarning2"} validationState={this.state.prodCodeValid}>
                    <FormControl type={"text"} placeholder={"Enter New Product Code (up to 10 character)"} inputRef={ref=>{this.newProd=ref;}} onChange={(e)=>{this.validNewProdCode(e.target.value)}}/>
                    <FormControl.Feedback/>
                </FormGroup>

            </Modal.Body>
            <Modal.Footer><Button onClick={()=>{this.addNewProd()}} bsStyle={"success"} disabled={this.state.btn4enable}>Continue</Button></Modal.Footer>
        </Modal>;

        return(
            <div className="navbar navbar-default">
               <div className="container">
                   {newProdCode}
                   <Form horizontal >
                       <FormGroup controlId={"formValidationWarning2"} validationState={this.state.codeValid}>
                           <Col md={6}>
                                   <FormControl type={"text"} placeholder={"Enter Product Code"} onChange={(e)=>{this.validProdCode(e.target.value)}} inputRef={ref=>{this.byCode=ref;}}/>
                                   <FormControl.Feedback />
                           </Col>
                           <Col md={1}>
                               <Button bsStyle={"primary"} href={this.getProductUrl()} disabled={this.state.btn1enable}><Glyphicon glyph={"search"}/></Button>
                           </Col>
                           <Col md={4} mdOffset={1}>
                               <ButtonToolbar>
                                   <Button bsStyle={"info"} href={"/products"}>All Products List</Button>
                                   <Button bsStyle={"success"} onClick={()=>{this.setState({newProdModal:true})}}>Add new Product</Button>
                               </ButtonToolbar>
                           </Col>

                       </FormGroup>


                   </Form>
               </div>
            </div>
        );
    }
}

ProductHead.propTypes={

};
