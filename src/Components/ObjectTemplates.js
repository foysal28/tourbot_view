
var template={
    product:{"prodCode": "",
        "description": "",
        "displayCategory": "",
        "vendor": {
            "vendorID":'',
            "vendorName": "",
        },
        "webPriority": "",
        "allotCode": "",
        "productDescription": "",
        "calendarPriority": "",
        "permRoomCode": false,
        "specialRoomFlag": false,
        "extranet": false,
        "roomTypeUpdateFlag": false,
        "transferType": '',
        "notifyEmails": "",
        "lastCalendarUpdate": null,
        "productConfiguration": {
            "multiple": '',
            "confType": '',
            "markTUISpain": false,
            "markTUIGermany": false,
            "htmlFile": null,
            "associated": false,
            "hotel": false
        },
        "productFlightInfo": {
            "flight1": "",
            "flight2": "",
            "flight3": "",
            "flight4": ""
        },
        "productFlags": {
            "misc1Flag": false,
            "misc2Flag": false,
            "pULocFlag": false,
            "pUTimeFlag": false,
            "flightItinFlag": false,
            "packageFlag": false,
            "allotmentFlag": false,
            "priceOverrideFlag": false,
            "displayOccFlag": false,
            "displayNightsFlag": false,
            "reportAllotUsageFlag": false
        },
        "productInstructions": {
            "instr1": "",
            "instr2": "",
            "instr3": "",
            "instr4": "",
            "instr5": ""
        },
        "webAccess": "",
        "promotionalDetails": {
            "brochureData": false,
            "eliteBrochureData": false,
            "freeSellProgram": false,
            "freeSellNotes": "",
            "usesLanguage": false
        },
        "dept": {
            "id":'' ,
            "label": ""
        },
        "cancellationPolicies": [
            {
                "ordinal": '',
                "daysPrior": '',
                "type": "Nights",
                "amount": ''
            },
            {
                "ordinal": '',
                "daysPrior": '',
                "type": "Percent",
                "amount": ''
            },
            {
                "ordinal": '',
                "daysPrior": '',
                "type": "Percent",
                "amount": ''
            }
        ],
        "priceProperties": {
            "needsPriceUpdate": false,
            "productCharge": '',
            "nonRefundable": false,
            "prodCom": false
        },
        "mealPlan": {
            "mealPlanID": '',
            "mealPlan": "",
            "dateUpdated": null,
            "deleted": false,
            "otaMealPlanCode": ''
        },
        "roomType": {
            "roomTypeId": '',
            "roomType": "",
            "dateUpdated": null,
            "deleted": false,
            "hiltonRoomTypeId": null,
            "hiltonHotelCodes": null,
            "externalRoomTypeId": null
        },
        "groupPricingType": null,
        "webDuration": {
            "startDate": null,
            "endDate": null
        },
        "occupancyConfiguration": {
            "defaultNights": '',
            "minNights": '',
            "maxOcc": '',
            "maxChildAge": ''
        },
        "resortFee": {
            "resortFeeID": '',
            "resortFeeType": null,
            "resortFee": ""
        },
        "roomTypeModified": null,
        "notifyDuration": {
            "startDate": null,
            "endDate": null
        },
        "disneyInformation": {
            "reportDisneyTicketFlag": '',
            "disneyHotelCode": "",
            "disneyRoomType": "",
            "disneyCodeCurrentYear": "",
            "disneyCodeNextYear": "",
            "disneyPackages": [
                {
                    "ordinal": '',
                    "disneyPackageCode": "",
                    "disneyAdultTicketCode": "",
                    "disneyChildTicketCode": "",
                    "disneyCodeDuration": {
                        "startDate": null,
                        "endDate": null
                    }
                },
                {
                    "ordinal": '',
                    "disneyPackageCode": "",
                    "disneyAdultTicketCode": "",
                    "disneyChildTicketCode": "",
                    "disneyCodeDuration": {
                        "startDate": null,
                        "endDate": null
                    }
                }
            ],
            "latestDisneyCalendar": null,
            "latestDisneyCalendarUpdate": null,
            "reportDisneyFlag": false
        },
        "productCategory": {
            "id": '',
            "label": ""
        },
        "restrictionProperties": {
            "dateChangeRestricted": false,
            "occChangeRestricted": false,
            "nightsChangeRestricted": false,
            "qtyChangeRestricted": false,
            "typeChangeRestricted": false,
            "priceChangeRestricted": false,
            "paxChangeRestricted": false,
            "webRestricted": false,
            "b2cRestricted": false
        },
        "modifierType": "No_Modifier",
        "activeProduct": false,
        "roundTrip": false},
};


export default template;