import React from 'react';
import PropTypes from 'prop-types';
import {FormControl,} from "react-bootstrap";
export class SelectList extends React.Component{
    constructor(){
        super();
        this.state={
          id:'',
          value:'',
        };
    }

    render(){
        let options=this.props.options.map((option,i)=>{
            return <option key={i} value={option.val} className="option">{option.name}</option>
        });
        // {console.log("defo",this.props.default)}
        let defOption=(this.props.default!==null)?<option defaultValue>{this.props.default}</option>:null;
        return(
            <div >
                <FormControl componentClass={"select"} inputRef={ref=>this.select=ref} onChange={(e)=>{this.props.getVal(e.target.value)}}>
                    {defOption}
                    {options}
                </FormControl>
            </div>
        );
    }
}

SelectList.propTypes={
    options:PropTypes.array,
    getVal:PropTypes.func,
    default:PropTypes.string,

};