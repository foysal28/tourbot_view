import React, { Component } from 'react';
import './App.css';
import Request from 'superagent';
import {Panel, Button, Glyphicon} from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import {ProductHead} from "./Components/ProductHead";
import {ProductList} from "./Components/ProductList";
import {ProductInfo} from "./Components/ProductInfo";

import 'react-datepicker/dist/react-datepicker.css';
import * as moment from "moment";

class App extends Component {
    static options =[{val:3,name:'All Product'},{val:0,name:'Dynamic (HBSI)'},{val:1,name:'Dynamic (Hilton)'},{val:2,name:'Static'}];
    constructor(){
        super();
        this.state={
            open:true,
            prodSelected:false,
            productList:[],
            productDetails:[],
            tstTime:'',

        };
    }
    // setProdList(prodlist){
    //
    //     this.setState({
    //         productList: prodlist,
    //         prodSelected:false,
    //     });
    // }
    setIcon(state){
        if(state) return <Glyphicon glyph={"menu-up"}/>
        else return <Glyphicon glyph={"menu-down"}/>
    }

    // callApi(){
    //     // fetch(`https://api.chucknorris.io/jokes/random`).then(result=>result.json()).then(products=>console.log({products}))
    //     Request.get(`https://api.chucknorris.io/jokes/random`).then(response=>console.log(response.body,"value: ",response.body.value))
    // }
    searchByCode(prodCode){
    let url=`http://4.28.134.168:8080/product/detail?_by_prodCode&id=`+prodCode;
        Request.get(url).accept('json').then(response=>{
           this.setState({
               productDetails:response.body,
               prodSelected:true,
           })
        });
    }
    searchByDesc(description,category,department){
        let url=`http://4.28.134.168:8080/product/list?_by_description=&description=`+description+`&department=`+department+`&category=`+category;
        console.log(url);
        Request.get(url).accept('json')
            .then(response=>{
                this.setState({
                    productList:new Array(response.body),
                    prodSelected:false
                });
            });
    }
    getAllProduct(){
        let url=`http://4.28.134.168:8080/product/list?_all&limit=100`;
        Request.get(url).accept('json').then(response=>{
            this.setState({
                productList: new Array(response.body),
                prodSelected:false
            });
        });


    }
    onSave(product){
        console.log(product);
    }

    dateFormat(date){
        console.log(date);
        console.log(moment("2017-10-14").utc().utcOffset(360));
        // console.log(date.getUTCDate());
        // console.log(date.getUTCMonth());
        // console.log(date.getUTCFullYear());
        // let tempObj=moment(date);
        // let tempDate='"'+tempObj.get('year')+'-'+tempObj.get('month')+1+'-'+tempObj.get('date')+'"';
        // return tempDate;
    }

  render() {
      let productBody= ((!this.state.prodSelected) && (this.state.productList.length>0))?<ProductList products={this.state.productList} />:((this.state.productDetails.length>0)?<ProductInfo prodDetails={this.state.productDetails} save={this.onSave.bind(this)}/>:<div></div>);
    return (
      <div className="container">
        <div className="row header">
          <Panel collapsible expanded={this.state.open}>
            <ProductHead option={App.options}
                         allProd={this.getAllProduct.bind(this)}
                         byProdCode={this.searchByCode.bind(this)}
                         byDesc={this.searchByDesc.bind(this)}
            />
          </Panel>
            <Button onClick={()=>{this.setState({open:!this.state.open})}}>{this.setIcon(this.state.open)}</Button>
        </div>
          {/*<DatePicker selected={moment("2017-1-14").utc().utcOffset(360)} dateFormat={"YYYY-MM-DD"} onChange={(date)=>{this.dateFormat(date)}}/>*/}
          {/*{console.log("date", ( new date(this.state.tstTime).getUTCMonth()+1))}*/}
          <div className="row">
              {productBody}
          </div>
      </div>
    );
  }
}

export default App;