import React,{ Component } from 'react';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';
import {ProductInfo} from "./Components/ProductInfo";
import * as Request from "superagent";
import {ProductHead} from "./Components/ProductHead";
import {Button, Glyphicon, Modal, Panel} from "react-bootstrap";
import {ProductList} from "./Components/ProductList";
import template from "./Components/ObjectTemplates";
import apiUrl from "./Components/BaseUrls";

class Routes extends Component {
    constructor(){
        super();
        this.state={
            list:true,
            panelOpen:true,
            productDetails:[],
            vendorList:[],
            description:'',
            vendorName:'',
            category:'',
            department:'',
            product:{},
        };
    }
     handleOnSave(product,flag){
        // let tmp=JSON.stringify(product);
        // let prd=JSON.parse(tmp);
        // console.log('onsave',prd);
         let url;

         switch (flag){
             case '':
                 url=apiUrl.update;
                 break;
             case 'clone':
                 url=apiUrl.clone+`copyAllotment=true&copyPricing=true&copyDefaultCancellation=true&copyWebAccess=true`;
                 break;
             case 'new':
                 url=apiUrl.save;
                 break;
         }
         Request.post(url).set('Content-Type', 'application/json').send(product)
             .end((err,res)=>{
                 if(err || !res.ok){
                     alert("Oops! "+err);
                 }
                 else{
                     alert("Product saved successfully")
                 }
             })


         //copyAllotment=true&copyPricing=true&copyDefaultCancellation=true&copyWebAccess=true
    }
    handleOnUpdate(product){

    }
    setIcon(state){
        if(state) return <Glyphicon glyph={"menu-up"}/>
        else return <Glyphicon glyph={"menu-down"}/>
    }
    // handleDescSearch(desc,dept,cat){
    //     this.setState({
    //         description:desc,
    //         department:dept,
    //         category:cat,
    //         vendorName:'',
    //     });
    //     // window.location.href=`/products`;
    // }
    // handleVendorSearch(vendor,dept,cat){
    //     this.setState({
    //         vendorName:vendor,
    //         department:dept,
    //         category:cat,
    //         description:''
    //     });
    //     // window.location.replace(`/products`);
    // }

    componentWillMount(){

        this.setState({
            product:template.product
        })
        let vendor_url=apiUrl.vendor;
        let vendorList=[];
        // Request.get(vendor_url).accept('json').end((err,res)=>{
        //         if(err || !res.ok){
        //             alert("Oops"+err);
        //         }else{
        //
        //          if(res.body.success){
        //              let tmp=JSON.parse(res.body.response);
        //               vendorList.push(tmp.map(vendor=>{return({"val":vendor.value,"name":vendor.label})}))
        //
        //          }
        //
        //         }
        //     }
        // );
    }
    newEmptyProd(str){
        let tmp={...template.product};
        tmp.prodCode=str;
        return tmp;
    }
    $

static test=({match})=>{console.log(match.params.prodCode);return <h1>found {match.params.prodCode}</h1>}

    render(){
        const prodInfo=({match})=>(
            <div>
                <Panel collapsible expanded={this.state.panelOpen}>
                    <ProductHead />
                </Panel>
                <Button onClick={()=>{this.setState({panelOpen:!this.state.panelOpen})}}>{this.setIcon(this.state.panelOpen)}</Button>
                <ProductInfo prodCode={match.params.prodCode} save={this.handleOnSave.bind(this)} update={this.handleOnUpdate.bind(this)}/>
            </div>
        );
        const prodList=()=>(
            <div>
                <Panel collapsible expanded={this.state.panelOpen}>
                    <ProductHead/>
                </Panel>
                <Button onClick={()=>{this.setState({panelOpen:!this.state.panelOpen})}}>{this.setIcon(this.state.panelOpen)}</Button>
                <ProductList description={this.state.description} vendorName={this.state.vendorName} department={this.state.department} category={this.state.category}/>
            </div>

        );
        const newProdInfo=({match})=>(
                <div>
                    <Panel collapsible expanded={this.state.panelOpen}>
                        <ProductHead/>
                    </Panel>
                    <Button onClick={()=>{this.setState({panelOpen:!this.state.panelOpen})}}>{this.setIcon(this.state.panelOpen)}</Button>
                    <ProductInfo prodTemplate={this.newEmptyProd(match.params.prodCode)} newProduct={true} save={this.handleOnSave.bind(this)}/>
                </div>
            );
        const justProdList=()=>(
            <div>
                <ProductList/>
            </div>
        );

        return(
            <Router>
                <div>
                    {/*{console.log("vendorName: ",this.state.vendorName," description: ",this.state.description)}*/}
                    {/*{console.log(template.product)}*/}
                    <div className={"container"}>
                        <div id={"home"} className={"full reveal"} data-reveal>

                        </div>

                        <Route path={"/prodCode=:prodCode"} component={prodInfo}/>
                        <Route path={"/products"} component={prodList}/>
                        <Route path={`/newProduct=:prodCode`} component={newProdInfo}/>
                        <Route path={"/list"} component={justProdList}/>
                    </div>
                </div>
            </Router>
        );
}

}
export default Routes;